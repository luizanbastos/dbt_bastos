{% macro grant_select_on_all_tables_for_database(database, group) %}
    
    {% call statement('st1', fetch_result=True, auto_begin=False) -%}
        show tables in {{ database }}
    {% endcall %}

    {% set tables = load_result('st1').data %}

    {% for db, table, x in tables %}
      {% call statement('st2', fetch_result=True, auto_begin=False) -%}
        GRANT SELECT ON {{ database }}.{{ table }} TO `{{ group }}`;
      {% endcall %}
    {% endfor %}

{% endmacro %}