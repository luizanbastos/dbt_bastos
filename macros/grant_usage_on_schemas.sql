{% macro grant_usage_on_schemas(schemas, user) %}
  {% for schema in schemas %}
    GRANT USAGE, SELECT, READ_METADATA ON DATABASE {{ schema }} TO {{ user }};
  {% endfor %}
{% endmacro %}