{% macro grant_select_on_model_to_users(model, users) %}
  {% for user in users %}
    {% call statement('grant_select_on_model', fetch_result=False, auto_begin=True) -%}
    --   GRANT USAGE  ON {{ model }} TO {{ user }};
      GRANT SELECT ON {{ model }} TO {{ user }};
    {% endcall %}
  {% endfor %}
  SELECT 1;
{% endmacro %}