# Sample Project: dbt_bastos

This project aims to build a sample data modelling structure to show how to 
build a data modelling structure using Amazon Redshift as Data Platform to 
process information, DBT to structure data modelling and Preset as analytics interface.

In this project, it was used some sample data about transactions as source data and a calendar
table, created for the porpouse of this project. The transaction data was available on a parquet file. 
Bellow there is a description of the steps used to build the project:


### Setting Infrastructure Up
1. Set up Redshift:

    a. Create an AWS account.

    b. Create a serverless cluster.

    c. Create a database.

    d. Create tables to upload data, defining their schema.

    e. Create and configure an IAM role and add it to the serverless cluster.

2. Set up an S3 Bucket and upload a Parquet file.
3. Ensure that the Parquet file and Redshift created tables have the same schema.
4. Copy content from the S3 Bucket to Redshift.
5. Create a DBT Cloud project.
6. Set up a connection with DBT Cloud and Redshift:

    a. Configure the Host, Port, and Database on DBT.

    b. Configure Access Control on Redshift Serverless with E2C Security Groups.

    c. Create a DBT user on the Amazon Redshift cluster.

    d. Configure database access and schema/table privileges for the DBT user.

7. Configure DBT jobs:

    a. Since developer licensing for DBT only offers one job run, the project only has a production job, configured to build all models and run all tests.

    b. The best practice is to have multiple jobs to configure a CI/CD deployment model, but it was not done for this sample project due to licensing limitations.

        i. CI jobs should consider data aspects connected to the development of the infrastructure and aim to test and find errors before they go to production. They should also be performant, aiming to test only code that has been changed.

        ii. CD jobs should consider the quality of the production environment and assure an adequate latency to analytics data, considering factors like scheduling to run on a constant basis and source table freshness.

### Data modelling
The data modelling for this project was build considering best practices from DBT and previous experience from the developer. 
The modelling layer has been separated on three layers:

#### Staging Layer
The objective here is to provide access to raw data in a simple manner, allowing data analysts/engineers to understand the underlying 
structure of the data. There are no changes on the schema neither application of logical transformations. Here we coerce the data to 
convenient data types allowing people with technical knowledge to investigate how the data arrives from the data sources.
In this layer, it's also applyed some basic tests that will allow technical responsibles to figure out more easily if a source 
of error on the data are from source or from the data development process and error handling.

#### Intermediate Layer
The objective here is to applymost of modelling logic, assuring that it meets business requirements. Technicaly is where most of the 
aggregations and conditional business rules should be. To develop this layer, there are considered the following principles:
1. Data modelling based on business entities

    a. First it's defined the main entities related to the business model. It's desirable that the ERD works like a map to the real business

    b. Business entities help to implement modularity, where most each entity has a sugested boundary of information that it should contain 
    (and each information should be contained on the scope only on the scope related to the business entity, making a paralell with S from SOLID 
    programing principles). This helps to structure the development process and aims to reduce future maintainance costs, since the modelling is
    conceptually more structured.

2. It's desirable that business entities are modelled with dimension and fact tables, generating a star schema.

    a. Dimensional modelling is a standard on data modelling industry and allow to new team members to grasp more easily the data modelling process.

    b. Dimensional modelling helps to structure data on a organized way, helping with group by statements on downstream tables

    c. It should be considered that dimensional modelling was optimal for older databases, but for the modern data stack, some products like Databricks
    and BigQuery don't grant optimal cost performance on this modelling schema. So dimensional modelling is a principle, but contextually can be broken

3. It's desirable that the code to build the data pipeline is DRY enought to avoid future maintenance

    a. DRYness reduces the chance of multiple code for each business rule

    b. DRYness reduces code complexity and probably improve query performance

On the intermediate layer, we also apply data tests. Here basic tests should aim to check if primary keys are unique and not null and if foreign keys 
relationship between tables are as expected. Some examples of more advanced and contextual tests are testing specific business rules and assuring that
the data is not changing with time. On this sample project only basic tests were applied.


#### Consumption Layer
The objective here is to provide tables ready for consumption on analytical interfaces (eg. Preset, Looker, PowerBI, Metabase). The design of this layer
should consider the necessity for business users acordingly to each business department, the cost and velocity query performance, 
the technical aspects of the selected analytical interface for the business, and planning for a friendly user interface to encourage 
self-service analytics. Here we also apply some tests, checking for if primary key are unique and if relationship between 
tables are as expected.

#### Analytical interface
The analytical interface for this project is configured to be used on Preset.

### Development Observations:
On the designed tests, there were found some errors due to transaction_id and user_id duplicity. Since the errors are relative to the data source,
 two measures could be applied considering a normal working context:

1. The errors should be treated on the data modelling, adding some code to treat them.

    a. The advantage is that 

        i. The analytics consumers will feel less impact on their consumption.

        ii. This probably will solve the issue in a short span of time.

    b. The disadvantages are that:

        i. The root cause of the error will not be treated by responsible teams and this enforces a culture of less ownership over 
        the data production.

        ii. The adicional code can become a technical debt over time.

2. The root cause of the error should be found and addressed

    a. The advantage are that:

        i. The analytics layer and systems that provide data will solve a technical debt

        ii. The culture of data ownership is being enforced

    b. The disadvantages are that:

        i. The analytics consumers will feel more impact on their consumption.

        ii. There will be a necessary effor to identify the root cause of the problem and to address it to the responsible team/person.

This decision should be treated considerig the tradeoff above. On the context of this project, we choose to not treat the data and to expose
the error to business users, considering that this measure enforces data ownership and that the effects of the test errors are not too big 
(on yml files we configured the erros to be warns and not errors).

