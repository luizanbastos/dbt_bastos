SELECT
  users_dim.user_id||'/'||CAST(users_fact.analysis_date AS VARCHAR) AS primary_key,
  users_dim.user_id,
  users_dim.user_state,
  users_dim.user_state_iso,
  users_dim.user_city,
  users_dim.first_transaction_at,
  users_fact.analysis_date,
  users_fact.previous_transaction_at_date,
  users_fact.days_without_transactions,
  users_fact.user_engagement_status,
  users_fact.churn_event_boolean,
  users_fact.daily_transactions_count,
  users_fact.daily_transaction_value
FROM {{ ref('int_users_dim') }} AS users_dim
LEFT JOIN {{ ref('int_users_fact') }} AS users_fact 
  ON users_dim.user_id = users_fact.user_id