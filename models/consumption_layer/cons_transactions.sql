SELECT
  transactions_fact.transaction_id,

  -- Dimensions
  transactions_dim.capture_method,
  transactions_dim.card_flag,
  transactions_dim.payment_method,
  transactions_dim.transaction_state,

  -- Facts
  transactions_fact.transaction_at,
  transactions_fact.transaction_value,
  users.user_id,
  users.user_state,
  users.user_state_iso,
  users.user_city
FROM {{ ref('int_transactions_fact') }} AS transactions_fact
LEFT JOIN {{ ref('int_transactions_dim') }} AS transactions_dim 
  ON transactions_fact.transaction_id = transactions_dim.transaction_id
LEFT JOIN {{ ref('int_users_dim') }} AS users
  ON users.user_id = transactions_fact.user_id