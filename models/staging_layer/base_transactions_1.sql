{{
    config(
        materialized='view'
    )
}}

SELECT
  CAST(transaction_id AS BIGINT) AS transaction_id,
  CAST(transaction_at AS TIMESTAMP) AS transaction_at,
  CAST(capture_method AS VARCHAR) AS capture_method,
  CAST(card_flag AS VARCHAR) AS card_flag,
  CAST(payment_method AS VARCHAR) AS payment_method,
  CAST(transaction_state AS VARCHAR) AS transaction_state,
  CAST(transaction_value AS FLOAT) AS transaction_value,
  CAST(user_id AS BIGINT) AS user_id,
  CAST(user_state AS VARCHAR) AS user_state,
  CAST(user_city AS VARCHAR) AS user_city
FROM {{ source('stn_raw', 'transactions_1') }}