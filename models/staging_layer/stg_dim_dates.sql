{{
    config(
        materialized='view'
    )
}}

SELECT full_date FROM {{ source('stn_raw', 'dim_dates') }}