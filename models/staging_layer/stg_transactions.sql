SELECT 
  transaction_id,
  transaction_at,
  capture_method,
  card_flag,
  payment_method,
  transaction_state,
  transaction_value,
  user_id,
  user_state,
  user_city
FROM {{ ref('base_transactions_1') }}

UNION ALL

SELECT
  transaction_id,
  transaction_at,
  capture_method,
  card_flag,
  payment_method,
  transaction_state,
  transaction_value,
  user_id,
  user_state,
  user_city
FROM {{ ref('base_transactions_2') }}