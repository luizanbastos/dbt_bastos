WITH users_preparation AS(
SELECT
  user_id,
  MIN(CAST(transaction_at AS DATE)) AS user_first_transaction_at,
  MAX(CAST(transaction_at AS DATE)) AS user_last_transaction_at
FROM {{ ref('stg_transactions') }}
GROUP BY 1
),

users AS(
SELECT
  DISTINCT
  dates.full_date AS analysis_date,
  users.user_id,
  users.user_first_transaction_at,
  users.user_last_transaction_at
FROM users_preparation AS users
CROSS JOIN {{ ref('stg_dim_dates') }} AS dates
WHERE dates.full_date >= CAST(users.user_first_transaction_at AS DATE)
--   AND dates.full_date <= '2012-01-01'
  AND dates.full_date <= '2011-07-01' --Data do ultimo registro de uma transação
),

user_transactions_preparation AS(
SELECT
  user_id,
  CAST(transaction_at AS DATE) AS transaction_at_date,
  COUNT(transaction_id) AS daily_transactions_count,
  SUM(transaction_value) AS daily_transaction_value
FROM {{ ref('stg_transactions') }} AS transactions
GROUP BY 1,2
),

user_transactions AS(
SELECT
  user_id,
  transaction_at_date,
  LAG(transaction_at_date) OVER (PARTITION BY user_id ORDER BY transaction_at_date) AS previous_transaction_at_date, 
  --Esta coluna irá cria ilhas quando aplicarmos o join para obter datas com a tabela users. Precisaremos de um tratamento de preenchimento (ffill + bfill)
  daily_transactions_count,
  daily_transaction_value
FROM user_transactions_preparation
),

users_daily_activity AS(
SELECT
  users.user_id,
  users.analysis_date,
  CASE WHEN previous_transaction_at_date IS NOT NULL THEN analysis_date --Ajustando para data corrente
       WHEN previous_transaction_at_date IS NULL AND analysis_date >= user_last_transaction_at
            THEN user_last_transaction_at --implementando ffill
       ELSE MIN (transactions.previous_transaction_at_date) 
               OVER (PARTITION BY users.user_id ORDER BY users.analysis_date 
               ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) --implementando bfill
       END AS previous_transaction_at_date, -- Esta coluna poderia ser otimizada implementando as window functions fora do case when, mas estou deixando assim dado que o objetivo aqui não é obter uma query performatica, mas uma maior compreensão do código
  daily_transactions_count,
  daily_transaction_value
FROM users
LEFT JOIN user_transactions AS transactions 
  ON users.analysis_date = transactions.transaction_at_date
  AND users.user_id = transactions.user_id
)

SELECT
  user_id||'/'||CAST(analysis_date AS VARCHAR)as primary_key,
  user_id,
  analysis_date,
  previous_transaction_at_date,
  DATEDIFF('day',previous_transaction_at_date, analysis_date) AS days_without_transactions,
  CASE WHEN  DATEDIFF('day',previous_transaction_at_date, analysis_date) >= 30 THEN 'Inativo'
       ELSE 'Ativo' 
       END AS user_engagement_status,
  CASE WHEN DATEDIFF('day',previous_transaction_at_date, analysis_date) = 31 THEN TRUE
       ELSE NULL 
       END AS churn_event_boolean,
  daily_transactions_count,
  daily_transaction_value
  -- SUM(daily_transactions_count) OVER (PARTITION BY user_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumulative_transaction_count,
  -- SUM(daily_transaction_value) OVER (PARTITION BY user_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumulative_transaction_value
FROM users_daily_activity