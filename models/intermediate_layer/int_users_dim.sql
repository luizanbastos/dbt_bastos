

WITH distinct_users AS (
SELECT DISTINCT
  user_id,
  user_state,
  user_city
FROM {{ ref('stg_transactions') }}
),

user_first_transaction AS(
SELECT
  user_id,
  MIN(transaction_at) AS first_transaction_at
FROM {{ ref('stg_transactions') }}
GROUP BY 1
)

SELECT
  --PK
  users.user_id,

  --Dimensions
  users.user_state,
  CONCAT('BR-',users.user_state) AS user_state_iso,
  users.user_city
FROM distinct_users AS users
LEFT JOIN user_first_transaction ON user_first_transaction.user_id = users.user_id