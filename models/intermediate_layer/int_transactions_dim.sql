SELECT
  -- PK
  transaction_id,

  --Dimensions
  capture_method,
  card_flag,
  payment_method,
  transaction_state
FROM {{ ref('stg_transactions') }}