
SELECT
  -- PK
  transaction_id,
  --FK
  user_id,

  --facts
  transaction_at,
  transaction_value
FROM {{ ref('stg_transactions') }}